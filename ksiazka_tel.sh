#!/bin/bash

if [ "$1" = "-h" ] || [ "$1" = "-help" ] || [ "$1" = "-?" ] || [ "$1" = "-man" ] || [ "$1" = "-manual" ]
then
    cat pomoc.txt | less
else

praca=1
until [ "$praca" = "0" ]; do

clear
echo -e "\tKSIAZKA TELEFONICZNA\n"

echo -e "Co chcesz zrobic? :\n"
echo -e "\t1.Dodaj nowy kontakt;\n"
echo -e "\t2.Wyswietl kontakty;\n"
echo -e "\t3.Wyswietl posortowane dane;\n"
echo -e "\t4.Wyszukiwanie kontaktow;\n"
echo -e "\t9.Pomoc;\n"

echo -e  "\t0.Wyjscie z programu;\n"
echo "Podaj numer (1,2,3,4,9 lub 0):"

read numer

case $numer in

    1)
    praca=1
    ./dodawanie_danych.sh
     ;;
    2)
    praca=1
    clear
	    cat dane.txt | less ;;
    3)
    praca=1
    clear
    sort -d  dane.txt | less  ;;
    4)
    praca=1
    ./wyszukiwanie_kontaktow.sh
    
     ;; 
    9)
    praca=1
    clear
    cat pomoc.txt | less ;;
    
    0)
    praca=0
     echo -e "\nDziekuje za skorzystanie z programu" ;;
    *)
    praca=1
     echo -e "\n\tNie ma takiego wyboru"
     sleep 3 ;;
    
esac

done

fi



